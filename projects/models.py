from django.db import models
from django.contrib.auth import authenticate

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField("auth.User", related_name="projects")
    user = authenticate(username="", password="")

    def __str__(self):
        return self.name
